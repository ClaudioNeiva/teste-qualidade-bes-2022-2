package br.ucsal._20222.testequalidade.aula14;

import java.net.MalformedURLException;
import java.net.URL;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.windows.WindowsDriver;
import io.appium.java_client.windows.WindowsElement;

@TestInstance(Lifecycle.PER_CLASS)
public class CalculadoraTest {

	private WindowsDriver<WindowsElement> driver;

	@BeforeAll
	void setup() throws MalformedURLException {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("ms:waitForAppLaunch", "5");
		cap.setCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
		driver = new WindowsDriver<>(new URL("http://127.0.0.1:4723/"), cap);
	}

	@AfterAll
	void teardown() throws MalformedURLException {
	driver.quit();
	}
	
	@Test
	void testarSoma() {
		driver.findElementByName("Seven").click();
		driver.findElementByName("Plus").click();
		driver.findElementByName("Eight").click();
		driver.findElementByName("Equals").click();
		WindowsElement calculatorResult = driver.findElementByAccessibilityId("CalculatorResults");
		Assertions.assertEquals("Display is 15", calculatorResult.getText());
	}

	@Test
	void testarSubtracao() {
		driver.findElementByName("Nine").click();
		driver.findElementByName("Minus").click();
		driver.findElementByName("Seven").click();
		driver.findElementByName("Equals").click();
		WindowsElement calculatorResult = driver.findElementByAccessibilityId("CalculatorResults");
		Assertions.assertEquals("Display is 2", calculatorResult.getText());
	}

}

