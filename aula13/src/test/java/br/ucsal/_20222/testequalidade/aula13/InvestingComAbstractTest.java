package br.ucsal._20222.testequalidade.aula13;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

@TestInstance(Lifecycle.PER_CLASS)
abstract class InvestingComAbstractTest {

	protected WebDriver driver;
	protected WebDriverWait wait;

	@AfterAll
	void teardownClass(){
		driver.quit();
	}
	
	@Test
	void testarPesquisa() throws InterruptedException {
		// Abrir página do Investing.com
//		new Thread() {
//			@Override
//			public void run() {
//				driver.get("http://br.investing.com");
//				super.run();
//			}
//		}.start();
//		Thread.sleep(2000); 
		driver.get("http://br.investing.com");

		// Páginas html dentro do projeto
		// driver.get(String.valueOf(ImpostoWebTest.class.getResource("/webapp/imposto.html")));

		WebElement oneTrustCloseButton = driver.findElement(By.className("onetrust-close-btn-handler"));
		oneTrustCloseButton.click();

		// Preencher o input de "Pesquisar no site..."
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys("COGN3" + Keys.ENTER);

		// Obter o conteúdo da página
		Thread.sleep(5000); // WebDriverWait?
//		wait.until(ExpectedConditions.numberOfElementsToBe(By.id("impostoPagar"), 1));
		String conteudo = driver.getPageSource();

//		WebElement impostoAPagarInput = driver.findElement(By.id("impostoPagar"));
//		String impostAPagar = impostoAPagarInput.getAttribute("value");
//		Assertions.assertEquals("1776.36", impostAPagar);

		// Verificar se retorno inclui "Cogna Educação"
		Assertions.assertTrue(conteudo.contains("Cogna Educação"));
	}

}
