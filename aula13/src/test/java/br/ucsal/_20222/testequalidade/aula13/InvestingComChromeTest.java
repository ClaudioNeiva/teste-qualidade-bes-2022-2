package br.ucsal._20222.testequalidade.aula13;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class InvestingComChromeTest extends InvestingComAbstractTest {
	
	@BeforeAll
	void setupClass() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 20);
	}

}
