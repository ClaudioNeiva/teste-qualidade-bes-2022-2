package br.ucsal._20222.testequalidade.aula13;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

class InvestingComTest {

	@ParameterizedTest
	@MethodSource("carregarDriver")
	void testarPesquisa(WebDriver driver) throws InterruptedException {
		driver.get("http://br.investing.com");

		WebElement oneTrustCloseButton = driver.findElement(By.className("onetrust-close-btn-handler"));
		oneTrustCloseButton.click();

		//driver.findElement(By.className("searchText")).sendKeys("COGN3" + Keys.ENTER);
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys("COGN3" + Keys.ENTER);

		Thread.sleep(5000);
		String conteudo = driver.getPageSource();

		Assertions.assertTrue(conteudo.contains("Cogna Educação"));

		driver.quit();
	}

	private static Stream<Arguments> carregarDriver() {
		WebDriverManager.chromedriver().setup();
		ChromeDriver chromeDrive = new ChromeDriver();
		WebDriverManager.firefoxdriver().setup();
		FirefoxDriver firefoxDriver = new FirefoxDriver();
		return Stream.of(Arguments.of(chromeDrive), Arguments.of(firefoxDriver)); 
	}

}
