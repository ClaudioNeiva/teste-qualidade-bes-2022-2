Cenário: Informar nota do aluno
Dado que estou logado como professor
E estou na tela de caderneta
Quando clico em "informar nota"
E informo para nota o valor 10
E informo para nota o valor 5
E clico em "confirmar"
Então é exibida a mensagem "Nota armazenada com sucesso"