package br.ucsal._20222.testequalidade.aula12;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;

public class GenericSteps {

	@Given("estou na tela de $tela")
	public void irParaTela(String tela) {
		// Uma lógica que faça o link entre a string "tela" e uma URL 
	}
	
	@When("clico em $labelBotao")
	public void clicar(String labelBotao) {
		// Localizar o botão na tela
		// Acionar o botão localizado no passo anterior
	}
	
	@When("$valor para o campo de $nomeCampo")
	public void preencherCampoV2(String nomeCampo, String valor) {
		preencherCampo(nomeCampo, valor);
	}
	
	@When("informo para $nomeCampo o valor $valor")
	public void preencherCampo(String nomeCampo, String valor) {
		// Fazer um mapeamento entre nomeCampo e um identificador web válido (id, name, xpath) 
		// Localizar o campo a partir do nomeCampo
		// Fazer um sendKeys valor para o campo localizado no passo anterior
	}
	
}
