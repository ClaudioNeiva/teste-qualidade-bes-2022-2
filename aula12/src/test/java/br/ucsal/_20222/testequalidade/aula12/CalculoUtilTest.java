package br.ucsal._20222.testequalidade.aula12;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CalculoUtilTest {

	@ParameterizedTest
	@CsvSource({ "0,1", "1,1", "5,120" })
	void testarCalcularFatorial(int n, long fatorialEsperado) {
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);

	}

}
