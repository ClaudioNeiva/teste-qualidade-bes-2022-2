package br.ucsal._20202.testequalidade.aula03.util;

public class CalculoUtil {

	private static final Double LIMITE_COMISSAO_BASE = 1000000d;
	private static final Double PERCENTUAL_COMISSAO_BASE = 0.03d;
	private static final Double PERCENTUAL_COMISSAO_EXTRA = 0.01d;
	private static final Double VALOR_BONUS_FIXO = 1500d;

	public long calcularFatorial(int n) {
		long fatorial = 1;
		for (int i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

	public static Double calcularComissao(double vendasTotais, Boolean isVendedorGrandesClientes) {
		Double comissao = null;
		Double comissaoExtra = null;
		if (vendasTotais > LIMITE_COMISSAO_BASE) {
			comissaoExtra = (vendasTotais - LIMITE_COMISSAO_BASE) * PERCENTUAL_COMISSAO_EXTRA;
		}
		comissao = vendasTotais * PERCENTUAL_COMISSAO_BASE;
		if (isVendedorGrandesClientes) {
			comissao += comissaoExtra;
			comissao += VALOR_BONUS_FIXO;
		}
		return comissao;
	}

}
