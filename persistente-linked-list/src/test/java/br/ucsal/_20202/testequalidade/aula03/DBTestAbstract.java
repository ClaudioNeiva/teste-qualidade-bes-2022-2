package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;

import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

public abstract class DBTestAbstract {

	@BeforeAll
	static void setupClass() {
		Assumptions.assumeTrue(DbUtil.isConnectionValid());
	}

}
