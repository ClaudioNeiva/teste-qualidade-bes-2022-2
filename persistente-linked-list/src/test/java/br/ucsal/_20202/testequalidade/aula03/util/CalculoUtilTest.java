package br.ucsal._20202.testequalidade.aula03.util;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@TestInstance(Lifecycle.PER_CLASS)
class CalculoUtilTest {

	private CalculoUtil calculoUtil;

	@BeforeAll
	void setupClass() {
		System.out.println("setupClass...");
	}

	@AfterAll
	void tearDownClass() {
		System.out.println("tearDownClass...");
	}

	@BeforeEach
	void setup() {
		System.out.println("setupMethod...");
		calculoUtil = new CalculoUtil();
	}

	@AfterEach
	void tearDown() {
		System.out.println("tearDownMethod...");
		calculoUtil = new CalculoUtil();
	}

	@Test
	void testarCalculoFatorial0() {
		System.out.println("testarCalculoFatorial0...");
		// Definir dados de entrada
		int n = 0;

		// Definir a saída esperada
		long fatorialEsperado = 1;

		// Executar o método que está sendo testado + obter a saída atual (resultado)
		long fatorialAtual = calculoUtil.calcularFatorial(n);

		// Comparar a saída esperada com a saída atual
		assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	@Disabled
	@DisplayName("Calculo do fatorial de 1")
	void testarCalculoFatorial1() {
		System.out.println("testarCalculoFatorial1...");
		// Definir dados de entrada
		int n = 1;

		// Definir a saída esperada
		long fatorialEsperado = 1;

		// Executar o método que está sendo testado + obter a saída atual (resultado)
		long fatorialAtual = calculoUtil.calcularFatorial(n);

		// Comparar a saída esperada com a saída atual
		assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	void testarCalculoFatorial5() {
		System.out.println("testarCalculoFatorial5...");
		// Definir dados de entrada
		int n = 5;

		// Definir a saída esperada
		long fatorialEsperado = 120;

		// Executar o método que está sendo testado + obter a saída atual (resultado)
		long fatorialAtual = calculoUtil.calcularFatorial(n);

		// Comparar a saída esperada com a saída atual
		assertEquals(fatorialEsperado, fatorialAtual);
	}

	@ParameterizedTest(name = "{index} - calcularFatorial({0})")
	@CsvSource(value = { "0:1", "1:1", "5:120" }, delimiter = ':')
	void testarFatoriais(int n, long fatorialEsperado) {
		long fatorialAtual = calculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

//	@ParameterizedTest(name = "{index} - testarMatricula({0},{1},{2})")
//	@MethodSource("fornecerDadosTest")
//	void testarMatricula(Aluno aluno, Disciplina disciplina, Professor professor) {
//		
//	}
//	
//	
//	void testarMatriculaCenario1(Aluno aluno, Disciplina disciplina, Professor professor) {
//		Aluno aluno1 = new Aluno(123,"claudio");
//		new Disciplina("BES008","POO",60), 
//		new Professor(789,"claudio neiva")
//		insert
//		
//	}
//	
//	private static Stream<Arguments> fornecerDadosTest() {
//		Aluno aluno1 = new Aluno(123,"claudio");
//	    return Stream.of(
//	  	      Arguments.of(aluno1, new Disciplina("BES008","POO",60), new Professor(789,"claudio neiva")),
//		      Arguments.of(aluno2, new Disciplina("BES009","Teste",60), new Professor(789,"mario ")),
//		      Arguments.of(aluno3, new Disciplina("BES018","GCM",60), new Professor(789,"fernando")),
//	    );
//	}

	
	@Test
	// Aqui é um uso ERRADO do assertAll! Porque são 3 casos de teste diferentes.
	void testarFatoriais() {
		assertAll(() -> assertEquals(1, calculoUtil.calcularFatorial(0)),
				() -> assertEquals(1, calculoUtil.calcularFatorial(1)),
				() -> assertEquals(120, calculoUtil.calcularFatorial(5)));
	}
	
	@Test
	void testarVendasCenario6() {
		double vendasTotais=1500000;
		Boolean isVendedorGrandesClientes = false;
		double comissaoEsperada = 45000;
		double comissaoAtual = CalculoUtil.calcularComissao(vendasTotais, isVendedorGrandesClientes);
		assertEquals(comissaoEsperada, comissaoAtual);
		
	}
	
}
