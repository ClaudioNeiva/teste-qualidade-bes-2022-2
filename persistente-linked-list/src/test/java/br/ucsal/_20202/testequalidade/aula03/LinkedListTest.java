package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

class LinkedListTest {

	private List<String> nomes;

	@BeforeEach
	void setup() {
		nomes = new LinkedList<>();
	}
	
	@Test
	void testarGetListaVazia() {
		Assertions.assertNull(nomes.get(0));
	}

	@Test
	void testarAddGet3Nomes() throws InvalidElementException {
		// Definir dados de entrada
		// Definir a saída esperada
		String nome0 = "antonio";
		String nome1 = "claudio";
		String nome2 = "neiva";

		// Executar o método que está sendo testado
		nomes.add(nome0);
		nomes.add(nome1);
		nomes.add(nome2);

		// Obter a saída atual (resultado)
		// Comparar a saída esperada com a saída atual
		Assertions.assertAll(() -> Assertions.assertEquals(nome0, nomes.get(0), "nomes(0)"),
				() -> Assertions.assertEquals(nome1, nomes.get(1), "nomes(1)"),
				() -> Assertions.assertEquals(nome2, nomes.get(2), "nomes(2)"));

	}

	@Test
	void testarAddElementInvalido() {
		// Dado de entrada - dado "inválido"
		String nomeInvalido = null;
		// Saída esperada, além da ocorrência da exceção
		String mensagemEsperada = "The element can't be null.";
		// Executar o método add e observar a ocorrência da exceção
		// Sucesso do teste é a ocorrência da exceção
		InvalidElementException exception = Assertions.assertThrows(InvalidElementException.class, () -> nomes.add(nomeInvalido));
		Assertions.assertEquals(mensagemEsperada, exception.getMessage());
	}

	void testarTamanhoListaVazia() {
	}
	
	void testarTamanhoLista5Elementos() {
	}
	
}
