package br.ucsal._20202.testequalidade.aula03;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

class PersitenteLinkedListTest extends DBTestAbstract {

	@Test
	void testarPersistirCargaLista1Nome() {

	}

	@Test
	void testarPersistirCargaLista10Nome() {

	}

	@Test
	void testarPersistirCargaLista3Nomes()
			throws InvalidElementException, SQLException, IOException, ClassNotFoundException {

		// Setup
		Connection connection = DbUtil.getConnection();
		PersistentList<String> nomes = new PersitenteLinkedList<>();
		Long idLista = 1L;
		String nomeTab = "tab_listas";

		// Dados de entrada
		String nome0 = "antonio";
		String nome1 = "claudio";
		String nome2 = "neiva";
		String nomeASerDescartado = "pedreira";

		// "Setup"
		nomes.add(nome0);
		nomes.add(nome1);
		nomes.add(nome2);

		// Executando o método que desejo testar
		nomes.persist(idLista, connection, nomeTab);

		// Resultado esperado (além dos nomes que devem estar na lista, e em ordem)
		Integer qtdElementosEsperado = 3;

		// Resultado atual
		PersistentList<String> nomesAtual = new PersitenteLinkedList<>();
		nomesAtual.add(nomeASerDescartado);
		nomesAtual.load(idLista, connection, nomeTab);

		// Comparar a saída esperada com a saída atual
		Assertions.assertAll(() -> Assertions.assertEquals(nome0, nomes.get(0), "nomes(0)"),
				() -> Assertions.assertEquals(nome1, nomes.get(1), "nomes(1)"),
				() -> Assertions.assertEquals(nome2, nomes.get(2), "nomes(2)"),
				() -> Assertions.assertEquals(qtdElementosEsperado, nomes.size()));
	}

}
