package br.ucsal.bes20222.testequalidade.locadora;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;

import br.ucsal.bes20222.testequalidade.builder.VeiculoBuilder;
import br.ucsal.bes20222.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.ClienteDAO;
import br.ucsal.bes20222.testequalidade.locadora.persistence.LocacaoDAO;
import br.ucsal.bes20222.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
@TestInstance(Lifecycle.PER_CLASS)
class LocacaoBOIntegradoTest {

	private static final String PLACA_VEICULO_ANTIGO_BARATO = "ABC-1234";
	private static final String PLACA_VEICULO_ANTIGO_CARO = "AZA-8765";

	private LocacaoBO locacaoBO;
	private VeiculoDAO veiculoDAO;
	private ClienteDAO clienteDAO;
	private LocacaoDAO locacaoDAO;

	private Veiculo veiculo1;
	private Veiculo veiculo2;
	private Cliente cliente;

	@BeforeAll
	void setupClass() {
		veiculoDAO = new VeiculoDAO();
		clienteDAO = new ClienteDAO();
		locacaoDAO = new LocacaoDAO();
		locacaoBO = new LocacaoBO(locacaoDAO, veiculoDAO, clienteDAO);

		criarVeiculosTest();
		criarClientesTest();
	}

	@BeforeEach
	void setup() {
		locacaoDAO.removeAll();
	}

	/**
	 * Testar o cálculo do valor total de locação por 4 dias de 2 veículos
	 * fabricados em 2015.
	 * 
	 * Caso de teste
	 * 
	 * # 1
	 * 
	 * Entrada: 2 veículos fabricados em 2015 (R$100,R150); anoAtual = 2022;
	 * qtdDiasLocacao = 4
	 * 
	 * Saída esperada: valor de locação = R$800
	 * 
	 * @throws VeiculoNaoEncontradoException
	 */
	@Test
	void testarCalculoValorTotalLocacao2Veiculos4Dias() throws VeiculoNaoEncontradoException {
		// Dados de entrada
		List<String> placas = Arrays.asList(PLACA_VEICULO_ANTIGO_CARO, PLACA_VEICULO_ANTIGO_BARATO);
		LocalDate dataReferencia = LocalDate.of(2022, 10, 10);
		Integer quantidadeDiasLocacao = 4;

		// Saída esperada
		Double valorLocacaoEsperado = 800.;

		// Executar o método que desejo testar e obter o resultado atual
		Double valorLocacaoAtual = locacaoBO.calcularValorTotalLocacao(placas, quantidadeDiasLocacao, dataReferencia);

		// Comparar o dado de entrada com a sáida esperada
		Assertions.assertEquals(valorLocacaoEsperado, valorLocacaoAtual);

	}

	@Test
	void testarSalvarLocacao() throws VeiculoNaoEncontradoException, ClienteNaoEncontradoException {
		List<String> placas = Arrays.asList(veiculo1.getPlaca(), veiculo2.getPlaca());
		Integer qtdDiasLocacao = 4;
		LocalDate dataLocacao = LocalDate.of(2022, 10, 10);

		locacaoBO.salvar(cliente.getCpf(), placas, qtdDiasLocacao, dataLocacao);

		// Resultado esperado? Como comparar o resultado do salvar com o resultado que
		// espero?
		List<Locacao> locacoes = locacaoDAO.findAll();
		Locacao locacaoEsperada = new Locacao(cliente, Arrays.asList(veiculo1, veiculo2), dataLocacao, qtdDiasLocacao,
				576.);
		Assertions.assertAll(() -> Assertions.assertEquals(1, locacoes.size()),
				() -> Assertions.assertEquals(locacaoEsperada, locacoes.get(0)));
	}

	private void criarVeiculosTest() {
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculoDisponivel().fabricadoEm(2015);
		veiculo1 = veiculoBuilder.mas().comPlaca("JPJ-1086").comValorDiaria(100.).build();
		veiculo2 = veiculoBuilder.mas().comPlaca("ABC-1234").comValorDiaria(80.).build();
		veiculoDAO.insert(veiculo1);
		veiculoDAO.insert(veiculo2);
	}

	private void criarClientesTest() {
		// TODO Criar ClienteBuilder.
		cliente = new Cliente("1234", "Claudio", "1234-21324");
		clienteDAO.insert(cliente);
	}

}
