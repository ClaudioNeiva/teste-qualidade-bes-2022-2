package br.ucsal.bes20222.testequalidade.builder;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

@Deprecated
public class VeiculoObjectMother {
	
	private VeiculoObjectMother() {
	}
	
	public static Veiculo umVeiculoAntigoDisponivel() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("CDE1234");
		veiculo.setAnoFabricacao(2015);
		veiculo.setValorDiaria(50.00);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		veiculo.setModelo(ModeloObjectMother.umModeloBasico());
		return veiculo;
	}

	public static Veiculo umVeiculoCaroAntigoDisponivel() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("CDE1234");
		veiculo.setAnoFabricacao(2015);
		veiculo.setValorDiaria(50.00);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		veiculo.setModelo(ModeloObjectMother.umModeloBasico());
		return veiculo;
	}

	public static Veiculo umVeiculoBaratoAntigoDisponivel() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("ABC1234");
		veiculo.setAnoFabricacao(2015);
		veiculo.setValorDiaria(500.20);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		veiculo.setModelo(ModeloObjectMother.umModeloLuxo());
		return veiculo;
	}

	
}
