package br.ucsal.bes20222.testequalidade.locadora;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import br.ucsal.bes20222.testequalidade.builder.VeiculoBuilder;
import br.ucsal.bes20222.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.ClienteDAO;
import br.ucsal.bes20222.testequalidade.locadora.persistence.LocacaoDAO;
import br.ucsal.bes20222.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
@TestInstance(Lifecycle.PER_CLASS)
class LocacaoBOTest {

	private static final String PLACA_VEICULO_ANTIGO_BARATO = "ABC-1234";
	private static final String PLACA_VEICULO_ANTIGO_CARO = "AZA-8765";

	@Mock
	private VeiculoDAO veiculoDAOMock;
	@Mock
	private LocacaoDAO locacaoDAOMock;
	@Mock
	private ClienteDAO clienteDAOMock;

	// @InjectMocks
	private LocacaoBO locacaoBOSpy;

	private Veiculo veiculo1;
	private Veiculo veiculo2;

	@BeforeAll
	void setupClass() throws Exception {
		// veiculoDAOMock = Mockito.mock(VeiculoDAO.class);
		MockitoAnnotations.openMocks(this).close();
		locacaoBOSpy = Mockito.spy(new LocacaoBO(locacaoDAOMock, veiculoDAOMock, clienteDAOMock));
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculoDisponivel().fabricadoEm(2015);
		veiculo1 = veiculoBuilder.mas().comPlaca(PLACA_VEICULO_ANTIGO_BARATO).comValorDiaria(100.00).build();
		veiculo2 = veiculoBuilder.mas().comPlaca(PLACA_VEICULO_ANTIGO_CARO).comValorDiaria(150.00).build();
	}

	/**
	 * Testar o cálculo do valor total de locação por 4 dias de 2 veículos
	 * fabricados em 2015.
	 * 
	 * Caso de teste
	 * 
	 * # 1
	 * 
	 * Entrada: 2 veículos fabricados em 2015 (R$100,R$150); anoAtual = 2022;
	 * qtdDiasLocacao = 4
	 * 
	 * Saída esperada: valor de locação = R$800
	 * 
	 * @throws VeiculoNaoEncontradoException
	 */
	@Test
	void testarCalculoValorTotalLocacao2Veiculos4Dias() throws VeiculoNaoEncontradoException {
		// Dados de entrada
		List<String> placas = Arrays.asList(PLACA_VEICULO_ANTIGO_CARO, PLACA_VEICULO_ANTIGO_BARATO);
		LocalDate dataReferencia = LocalDate.of(2022, 10, 10);
		Integer quantidadeDiasLocacao = 4;

		// Setup do stub
		Mockito.when(veiculoDAOMock.obterPorPlacas(placas)).thenReturn(Arrays.asList(veiculo1, veiculo2));

		// Saída esperada
		Double valorLocacaoEsperado = 800.;

		// Executar o método que desejo testar e obter o resultado atual
		Double valorLocacaoAtual = locacaoBOSpy.calcularValorTotalLocacao(placas, quantidadeDiasLocacao,
				dataReferencia);

		// Comparar o dado de entrada com a sáida esperada
		Assertions.assertEquals(valorLocacaoEsperado, valorLocacaoAtual);
	}

	@Test
	void testarSalvarLocacao() throws VeiculoNaoEncontradoException, ClienteNaoEncontradoException {
		// FIXME Preparar os dados para teste e os stubs adcionais (clienteDAOStub).
		String cpfCliente = "123";
		List<String> placas = Arrays.asList(PLACA_VEICULO_ANTIGO_CARO, PLACA_VEICULO_ANTIGO_BARATO);
		LocalDate dataReferencia = LocalDate.of(2022, 10, 10);
		Integer quantidadeDiasLocacao = 4;
		List<Veiculo> veiculos = Arrays.asList(veiculo1, veiculo2);

		// FIXME Criar o builder para o domain Cliente.
		Cliente cliente = new Cliente(cpfCliente, "claudio", "71987593667");

		// FIXME Criar o builder para o domain Locacao.
		Double valorLocacaoEsperado = 800.;
		Locacao locacaoEsperada = new Locacao(cliente, veiculos, dataReferencia, quantidadeDiasLocacao,
				valorLocacaoEsperado);

		Mockito.when(veiculoDAOMock.obterPorPlacas(placas)).thenReturn(veiculos);
		Mockito.when(clienteDAOMock.obterPorCpf(cpfCliente)).thenReturn(cliente);
//		Mockito.when(locacaoBOSpy.calcularValorTotalLocacao(placas, quantidadeDiasLocacao, dataReferencia))
//				.thenReturn(valorLocacaoEsperado);
		Mockito.doReturn(valorLocacaoEsperado).when(locacaoBOSpy).calcularValorTotalLocacao(placas,
				quantidadeDiasLocacao, dataReferencia);

		locacaoBOSpy.salvar(cpfCliente, placas, quantidadeDiasLocacao, dataReferencia);

		// Observando o estado final do sistema, para inferir se o salvar teve êxito.
		// O verify "é o assert".
		Mockito.verify(locacaoDAOMock).insert(locacaoEsperada);
	}

}
