package br.ucsal.bes20222.testequalidade.builder;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	private static final String DEFAULT_PLACA = "Abc-1234";
	private static final Integer DEFAULT_ANO_FABRICACAO = null;
	private static final Modelo DEFAULT_MODELO = ModeloBuilder.umModeloBasico().build();
	private static final Double DEFAULT_VALOR_DIARIA = null;
	private static final SituacaoVeiculoEnum DEFAULT_SITUACAO = null;

	private String placa = DEFAULT_PLACA;
	private Integer anoFabricacao = DEFAULT_ANO_FABRICACAO;
	private Modelo modelo = DEFAULT_MODELO;
	private Double valorDiaria = DEFAULT_VALOR_DIARIA;
	private SituacaoVeiculoEnum situacao = DEFAULT_SITUACAO;
	
	private VeiculoBuilder() {
	}
	
	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}

	public static VeiculoBuilder umVeiculoDisponivel() {
		return new VeiculoBuilder().disponivel();
	}

	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder fabricadoEm(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
		return this;
	}

	public VeiculoBuilder doModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuilder doModelo(String nomeModelo) {
		this.modelo = ModeloBuilder.umModelo().comNome(nomeModelo).build();
		return this;
	}

	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoBuilder disponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}

	public VeiculoBuilder emManutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder locado() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}

	public VeiculoBuilder naSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public VeiculoBuilder mas() {
		return new VeiculoBuilder().comPlaca(placa).comValorDiaria(valorDiaria)
				.fabricadoEm(anoFabricacao).doModelo(modelo).naSituacao(situacao);
	}

	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAnoFabricacao(anoFabricacao);
		veiculo.setModelo(modelo);
		veiculo.setSituacao(situacao);
		veiculo.setValorDiaria(valorDiaria);
		return veiculo;
	}

}
