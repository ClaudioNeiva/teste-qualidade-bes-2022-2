package br.ucsal.bes20222.testequalidade.locadora;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import br.ucsal.bes20222.testequalidade.builder.VeiculoBuilder;
import br.ucsal.bes20222.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.ClienteDAO;
import br.ucsal.bes20222.testequalidade.mock.LocacaoDAOMock;
import br.ucsal.bes20222.testequalidade.mock.VeiculoDAOStub;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
@TestInstance(Lifecycle.PER_CLASS)
class LocacaoBOSemMockitoTest {

	private static final String PLACA_VEICULO_ANTIGO_BARATO = "ABC-1234";
	private static final String PLACA_VEICULO_ANTIGO_CARO = "AZA-8765";

	private VeiculoDAOStub veiculoDAOStub;
	private LocacaoBO locacaoBO;
	private ClienteDAO clienteDAO;
	private LocacaoDAOMock locacaoDAOMock;

	private Veiculo veiculo1;
	private Veiculo veiculo2;

	@BeforeAll
	void setupClass() {
		veiculoDAOStub = new VeiculoDAOStub();
		clienteDAO = new ClienteDAO();
		locacaoDAOMock = new LocacaoDAOMock();
		locacaoBO = new LocacaoBO(locacaoDAOMock, veiculoDAOStub, clienteDAO);

		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculoDisponivel().fabricadoEm(2015);
		veiculo1 = veiculoBuilder.mas().comPlaca(PLACA_VEICULO_ANTIGO_BARATO).comValorDiaria(100.00).build();
		veiculo2 = veiculoBuilder.mas().comPlaca(PLACA_VEICULO_ANTIGO_CARO).comValorDiaria(150.00).build();
	}

	/**
	 * Testar o cálculo do valor total de locação por 4 dias de 2 veículos
	 * fabricados em 2015.
	 * 
	 * Caso de teste
	 * 
	 * # 1
	 * 
	 * Entrada: 2 veículos fabricados em 2015 (R$100,R$150); anoAtual = 2022;
	 * qtdDiasLocacao = 4
	 * 
	 * Saída esperada: valor de locação = R$800
	 * 
	 * @throws VeiculoNaoEncontradoException
	 */
	@Test
	void testarCalculoValorTotalLocacao2Veiculos4Dias() throws VeiculoNaoEncontradoException {
		// Dados de entrada
		List<String> placas = Arrays.asList(PLACA_VEICULO_ANTIGO_CARO, PLACA_VEICULO_ANTIGO_BARATO);
		LocalDate dataReferencia = LocalDate.of(2022, 10, 10);
		Integer quantidadeDiasLocacao = 4;

		// Setup do stub
		veiculoDAOStub.setup(Arrays.asList(veiculo1, veiculo2));

		// Saída esperada
		Double valorLocacaoEsperado = 800.;

		// Executar o método que desejo testar e obter o resultado atual
		Double valorLocacaoAtual = locacaoBO.calcularValorTotalLocacao(placas, quantidadeDiasLocacao, dataReferencia);

		// Comparar o dado de entrada com a sáida esperada
		Assertions.assertEquals(valorLocacaoEsperado, valorLocacaoAtual);

	}

	@Test
	void testarSalvarLocacao() throws VeiculoNaoEncontradoException, ClienteNaoEncontradoException {
		// FIXME Preparar os dados para teste e os stubs adcionais (clienteDAOStub).
		String cpfCliente = null;
		List<String> placas = null;
		Integer qtdDiasLocacao = null;
		LocalDate dataLocacao = null;
		
		locacaoBO.salvar(cpfCliente, placas, qtdDiasLocacao, dataLocacao);
		
		// Observando o estado final do sistema, para inferir se o salvar teve êxito.
		locacaoDAOMock.verificarChamadas("insert", 1);
	}
	
}
