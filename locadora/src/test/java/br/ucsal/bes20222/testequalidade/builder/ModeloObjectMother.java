package br.ucsal.bes20222.testequalidade.builder;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Modelo;

@Deprecated
public class ModeloObjectMother {

	private ModeloObjectMother() {
	}
	
	public static Modelo umModeloBasico() {
		return new Modelo("Gol");
	}
	
	public static Modelo umModeloLuxo() {
		return new Modelo("X4");
	}
	
}
