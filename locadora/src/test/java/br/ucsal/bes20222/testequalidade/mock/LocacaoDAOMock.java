package br.ucsal.bes20222.testequalidade.mock;

import java.util.HashMap;
import java.util.Map;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20222.testequalidade.locadora.persistence.LocacaoDAO;

public class LocacaoDAOMock extends LocacaoDAO {

	// Map<nome-metodo, qtd-chamadas>
	private Map<String, Integer> chamadas = new HashMap<>();

	@Override
	public void insert(Locacao locacao) {
		chamadas.putIfAbsent("insert", 0);
		chamadas.put("insert", chamadas.get("insert") + 1);
	}

	public void verificarChamadas(String nomeMetodo, Integer qtdEsperada) {
		if (!chamadas.containsKey(nomeMetodo) || chamadas.get(nomeMetodo) != qtdEsperada) {
			throw new RuntimeException(
					"Não foi chamado o método esperado ou a quantidad de chamadas foi diferente da esperada");
		}
	}

}
