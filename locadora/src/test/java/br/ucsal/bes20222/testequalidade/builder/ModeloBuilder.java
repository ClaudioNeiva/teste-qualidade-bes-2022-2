package br.ucsal.bes20222.testequalidade.builder;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Modelo;

public class ModeloBuilder {

	private static final String DEFAULT_NOME = null;

	private static final String NOME_MODELO_BASICO = "QQ";

	private String nome = DEFAULT_NOME;

	private ModeloBuilder() {
	}

	public static ModeloBuilder umModelo() {
		return new ModeloBuilder();
	}

	public static ModeloBuilder umModeloBasico() {
		return new ModeloBuilder().comNome(NOME_MODELO_BASICO);
	}

	public ModeloBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public Modelo build() {
		return new Modelo(nome);
	}
}
