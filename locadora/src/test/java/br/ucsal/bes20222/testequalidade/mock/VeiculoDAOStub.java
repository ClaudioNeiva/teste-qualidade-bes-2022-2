package br.ucsal.bes20222.testequalidade.mock;

import java.util.List;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.VeiculoDAO;

public class VeiculoDAOStub extends VeiculoDAO {

	private List<Veiculo> veiculosPreConfigurados;

	public void setup(List<Veiculo> veiculos) {
		this.veiculosPreConfigurados = veiculos;
	}

	@Override
	public List<Veiculo> obterPorPlacas(List<String> placas) throws VeiculoNaoEncontradoException {
		return veiculosPreConfigurados;
	}

}
