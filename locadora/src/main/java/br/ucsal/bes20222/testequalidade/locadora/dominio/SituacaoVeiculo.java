package br.ucsal.bes20222.testequalidade.locadora.dominio;

public class SituacaoVeiculo {

	private String codigo;

	private String descricao;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
