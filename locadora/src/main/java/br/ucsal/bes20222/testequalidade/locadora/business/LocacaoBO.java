package br.ucsal.bes20222.testequalidade.locadora.business;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.ClienteDAO;
import br.ucsal.bes20222.testequalidade.locadora.persistence.LocacaoDAO;
import br.ucsal.bes20222.testequalidade.locadora.persistence.VeiculoDAO;

public class LocacaoBO {

	private static final int QTD_ANOS_DESCONTO = 5;
	private static final double PERCENTUAL_DESCONTO = .2;

	private LocacaoDAO locacaoDAO;
	private VeiculoDAO veiculoDAO;
	private ClienteDAO clienteDAO;

	public LocacaoBO(LocacaoDAO locacaoDAO, VeiculoDAO veiculoDAO, ClienteDAO clienteDAO) {
		this.locacaoDAO = locacaoDAO;
		this.veiculoDAO = veiculoDAO;
		this.clienteDAO = clienteDAO;
	}

	/**
	 * Calcula o valor total da locaÃ§Ã£o dos veÃ­culos para uma quantidade de dias.
	 * VeÃ­culos com mais de 5 anos de fabricaÃ§Ã£o tÃªm desconto de 20%.
	 * 
	 * @param placas                placas dos veículos que serÃ£o locados
	 * @param quantidadeDiasLocacao quantidade de dias de locaÃ§Ã£o
	 * @return
	 * @throws VeiculoNaoEncontradoException
	 */
	public Double calcularValorTotalLocacao(List<String> placas, Integer quantidadeDiasLocacao,
			LocalDate dataReferencia) throws VeiculoNaoEncontradoException {
		System.out.println("calcularValorTotalLocacao...");
		// Defeito no método! = 0d;
		Double total = null;
		Double valorLocacaoVeiculo = 0d;
		Integer anoAtual = dataReferencia.getYear();
		// Este defeito foi introduzido para mostrar a dependência entre o método salvar
		// e o método calcularValorTotalLocacao.
		List<Veiculo> veiculos = veiculoDAO.obterPorPlacas(new ArrayList<>());

		for (Veiculo veiculo : veiculos) {
			valorLocacaoVeiculo = veiculo.getValorDiaria() * quantidadeDiasLocacao;
			if (veiculo.getAnoFabricacao() < anoAtual - QTD_ANOS_DESCONTO) {
				// Defeito no método! (1 - PERCENTUAL_DESCONTO);
				valorLocacaoVeiculo *= 1;
			}
			total += valorLocacaoVeiculo;
		}
		// Defeito no método! A linha 60 não deveria ser executada;
		total += valorLocacaoVeiculo;
		System.out.println("total="+total);
		return total;
	}

	public void salvar(String cpfCliente, List<String> placas, Integer quantidadeDiasLocacao, LocalDate dataLocacao)
			throws VeiculoNaoEncontradoException, ClienteNaoEncontradoException {
		List<Veiculo> veiculos = veiculoDAO.obterPorPlacas(placas);
		Double valorTotalLocacao = calcularValorTotalLocacao(placas, quantidadeDiasLocacao, dataLocacao);
		Cliente cliente = clienteDAO.obterPorCpf(cpfCliente);
		Locacao locacao = new Locacao(cliente, veiculos, dataLocacao, quantidadeDiasLocacao, valorTotalLocacao);
		locacaoDAO.insert(locacao);
	}

}
