package br.ucsal.bes20222.testequalidade.locadora.dominio;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Locacao {

	private static Integer seq = 0;

	private Integer numeroContrato;

	private Cliente cliente;

	private List<Veiculo> veiculos;

	private LocalDate dataLocacao;

	private Integer quantidadeDiasLocacao;

	private LocalDate dataDevolucao;

	private Double valorLocacao;

	public Locacao(Cliente cliente, List<Veiculo> veiculos, LocalDate dataLocacao, Integer quantidadeDiasLocacao,
			Double valorLocacao) {
		super();
		definirNumeroContrato();
		this.cliente = cliente;
		this.veiculos = veiculos;
		this.dataLocacao = dataLocacao;
		this.quantidadeDiasLocacao = quantidadeDiasLocacao;
		this.valorLocacao = valorLocacao;
	}

	public Integer getNumeroContrato() {
		return numeroContrato;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public LocalDate getDataLocacao() {
		return dataLocacao;
	}

	public Integer getQuantidadeDiasLocacao() {
		return quantidadeDiasLocacao;
	}

	public LocalDate getDataDevolucao() {
		return dataDevolucao;
	}

	private void definirNumeroContrato() {
		seq++;
		numeroContrato = seq;
	}

	public Double getValorLocacao() {
		return valorLocacao;
	}

	public void setValorLocacao(Double valorLocacao) {
		this.valorLocacao = valorLocacao;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cliente, dataDevolucao, dataLocacao, quantidadeDiasLocacao, valorLocacao, veiculos);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Locacao other = (Locacao) obj;
		return Objects.equals(cliente, other.cliente) && Objects.equals(dataDevolucao, other.dataDevolucao)
				&& Objects.equals(dataLocacao, other.dataLocacao)
				&& Objects.equals(quantidadeDiasLocacao, other.quantidadeDiasLocacao)
				&& Objects.equals(valorLocacao, other.valorLocacao) && Objects.equals(veiculos, other.veiculos);
	}

	@Override
	public String toString() {
		return "Locacao [numeroContrato=" + numeroContrato + ", cliente=" + cliente + ", veiculos=" + veiculos
				+ ", dataLocacao=" + dataLocacao + ", quantidadeDiasLocacao=" + quantidadeDiasLocacao
				+ ", dataDevolucao=" + dataDevolucao + ", valorLocacao=" + valorLocacao + "]";
	}

}
