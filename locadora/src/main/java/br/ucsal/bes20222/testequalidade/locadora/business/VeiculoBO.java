package br.ucsal.bes20222.testequalidade.locadora.business;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.persistence.VeiculoDAO;

public class VeiculoBO {

	private VeiculoDAO veiculoDAO;
	
	public VeiculoBO() {
		System.out.println("VeiculoBO construtor...");
	}
	
	public void salvar(Veiculo veiculo) {
		System.out.println("Vou salvar o veículo uma hora dessas...");
		System.out.println(veiculoDAO);
	}
}
